package com.cognizant.ormlearn.Application;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.cognizant.ormlearn.model.Country;



public class Application {

	public static void main(String args[]) {
		AnnotationConfiguration configuration = new AnnotationConfiguration();
		configuration.configure("hibernate.cfg.xml").addAnnotatedClass(Country.class);
		SessionFactory sessionFactory=configuration.buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		Country country = new Country("AS", "American Samoa");
		session.save(country);
		transaction.commit();
	}}
