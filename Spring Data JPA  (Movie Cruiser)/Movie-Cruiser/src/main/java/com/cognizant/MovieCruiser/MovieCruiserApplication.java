package com.cognizant.MovieCruiser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cognizant.MovieCruiser.Service.FavMovieService;
import com.cognizant.MovieCruiser.Service.MovieService;
import com.cognizant.MovieCruiser.Service.UserService;
import com.cognizant.MovieCruiser.model.Fav_movie;
import com.cognizant.MovieCruiser.model.Movie;
import com.cognizant.MovieCruiser.model.User;
import com.cognizant.MovieCruiser.util.DateUtil;

@SpringBootApplication
public class MovieCruiserApplication {
	public static UserService service;
	public static MovieService movieService;
	public static FavMovieService favMovieService;

	public static void main(String[] args) {
		ApplicationContext run = SpringApplication.run(MovieCruiserApplication.class, args);
		service = run.getBean(UserService.class);
		movieService = run.getBean(MovieService.class);
		favMovieService = run.getBean(FavMovieService.class);

//		testapp1();
//		testapp2();
//		testapp3();
		System.out.println("------------------END------------------");
	}

	public static void testapp1() {
		service.save(new User(2, "Prayas", "abc", "prayas", "vijayvargiya", "prayas@abc.com", "user"));
		System.out.println(service.findAll());

	}

	public static void testapp2() {

		movieService.save(new Movie(1, "ff7", "action", DateUtil.convertToDate("10/10/2010"), 1000d, false, false, null,
				false, "abc.jpg"));
		System.out.println("all"+movieService.findAll());
		System.out.println("id"+movieService.findById(1));
		System.out.println("search"+movieService.searchMovie("f"));

	}

	public static void testapp3() {
		favMovieService.save(new Fav_movie(1));

		System.out.println(favMovieService.findAll());

	}
}
