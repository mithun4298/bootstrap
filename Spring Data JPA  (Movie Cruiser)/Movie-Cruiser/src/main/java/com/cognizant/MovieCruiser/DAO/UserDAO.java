package com.cognizant.MovieCruiser.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cognizant.MovieCruiser.model.User;

@Repository
public interface UserDAO extends JpaRepository<User, Integer> {

	@Query("FROM User WHERE username=:username and password=:password")
	public User getUserByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

	@Query("FROM User WHERE username=:username ")
	public List<User> findUserByUsername(@Param("username") String username);

}
