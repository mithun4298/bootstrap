package com.cognizant.MovieCruiser.Service;

import java.util.List;

import com.cognizant.MovieCruiser.Service.Exception.MovieException;
import com.cognizant.MovieCruiser.model.Movie;

public interface MovieService {

	public List<Movie> findAll();

	public Movie findById(int id);

	public List<Movie> findByTitle(String Title);

	public void save(Movie movie);

	public void updateMovie(Movie movie) throws MovieException;

	public void deleteById(int id);

	public List<Movie> searchMovie(String find);

}
